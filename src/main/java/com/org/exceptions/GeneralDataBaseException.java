package com.org.exceptions;

public class GeneralDataBaseException extends RuntimeException {
    public GeneralDataBaseException(String s) {
        super(s);
    }

}