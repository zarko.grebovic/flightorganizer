package com.org.configuration;

import javassist.NotFoundException;
import org.apache.log4j.Logger;
import org.h2.jdbc.JdbcSQLException;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ValidationException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Optional.ofNullable;

@ControllerAdvice
public class GlobalExceptionHandler {

    static final Logger logger = Logger.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<String> handleNotFoundException(HttpServletRequest request, NotFoundException ex) {
        logger.error("NotFoundException " + request.getRequestURI());

        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(("Not found exception\nException details you can see below\n\n" + ex.getMessage()));
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<String> handleValidationException(HttpServletRequest request, ValidationException ex) {
        logger.error("ValidationException " + request.getRequestURI(), ex);

        return ResponseEntity
                .badRequest()
                .body("Validation exception\nException details you can see below\n\n" + ex.getMessage());
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<String> handleMissingServletRequestParameterException(HttpServletRequest request, MissingServletRequestParameterException ex) {
        logger.error("handleMissingServletRequestParameterException " + request.getRequestURI(), ex);

        return ResponseEntity
                .badRequest()
                .body(("Missing request parameter\nException details you can see below\n\n" + ex.getMessage()));
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<String> handleMethodArgumentTypeMismatchException(HttpServletRequest request, MethodArgumentTypeMismatchException ex) {
        logger.error("handleMethodArgumentTypeMismatchException " + request.getRequestURI(), ex);

        Map<String, String> details = new HashMap<>();
        details.put("paramName", ex.getName());
        details.put("paramValue", ofNullable(ex.getValue()).map(Object::toString).orElse(""));
        details.put("errorMessage", ex.getMessage());

        return ResponseEntity
                .badRequest()
                .body(("Method argument type mismatch\nException details you can see below\n\n" + details));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<String> handleMethodArgumentNotValidException(HttpServletRequest request, MethodArgumentNotValidException ex) {
        logger.error("handleMethodArgumentNotValidException " + request.getRequestURI(), ex);

        List<Map<String, String>> details = new ArrayList<>();
        ex.getBindingResult()
                .getFieldErrors()
                .forEach(fieldError -> {
                    Map<String, String> detail = new HashMap<>();
                    detail.put("objectName", fieldError.getObjectName());
                    detail.put("field", fieldError.getField());
                    detail.put("rejectedValue", "" + fieldError.getRejectedValue());
                    detail.put("errorMessage", fieldError.getDefaultMessage());
                    details.add(detail);
                });

        return ResponseEntity
                .badRequest()
                .body(("Method argument validation failed\nException details you can see below\n\n" + details));
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<String> handleAccessDeniedException(HttpServletRequest request, AccessDeniedException ex) {
        logger.error("handleAccessDeniedException " + request.getRequestURI(), ex);

        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .body("Access denied!\nException details you can see below\n\n" + ex.getMessage());
    }

    @ExceptionHandler(SQLException.class)
    public ResponseEntity<String> handleSqlException(HttpServletRequest request, SQLException ex) {
        logger.error("handleSqlException " + request.getRequestURI(), ex);

        return ResponseEntity
                .badRequest()
                .body(("Database problem message\nException details you can see below\n\n" + ex.getMessage()));
    }

    @ExceptionHandler(JdbcSQLException.class)
    public ResponseEntity<String> handleJdbcSqlException(HttpServletRequest request, JdbcSQLException ex) {
        logger.error("handleJdbcSqlException " + request.getRequestURI(), ex);

        return ResponseEntity
                .badRequest()
                .body(("JDBC problem message\nException details you can see below\n\n" + ex.getMessage()));
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<String> handleIOException(HttpServletRequest request, IOException ex) {
        logger.error("handleIOException " + request.getRequestURI(), ex);

        return ResponseEntity
                .badRequest()
                .body(("IO problem occurred\nException details you can see below\n\n" + ex.getMessage()));
    }

    @ExceptionHandler(DataAccessException.class)
    public ResponseEntity<String> handleDataAccessException(HttpServletRequest request, DataAccessException ex) {
        logger.error("handleDataAccessException " + request.getRequestURI(), ex);

        return ResponseEntity
                .badRequest()
                .body(("Data Access Problem occurred\nException details you can see below\n\n" + ex.getMessage()));
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<String> handleDataIntegrityViolationException(HttpServletRequest request, DataIntegrityViolationException ex) {
        logger.error("handleDataIntegrityViolationException " + request.getRequestURI(), ex);

        return ResponseEntity
                .badRequest()
                .body(("Data Violation Exception occurred\nException details you can see below\n\n" + ex.getMessage()));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleInternalServerError(HttpServletRequest request, Exception ex) {
        logger.error("handleInternalServerError " + request.getRequestURI(), ex);

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");
        headers.add("Access-Control-Allow-Origin", "*");

        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .headers(headers)
                .body("Internal server error\nException details you can see below\n\n" + ex.getMessage());
    }


}



