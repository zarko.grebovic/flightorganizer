package com.org.configuration;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CORSFilter implements Filter {

    protected static final Logger LOGGER = Logger.getLogger(CORSFilter.class);

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
	    throws IOException, ServletException {
	LOGGER.info("CORS Filtering on......................................");
	HttpServletResponse response = (HttpServletResponse) res;
	response.addHeader("Access-Control-Allow-Origin", "*");
	response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
	response.setHeader("Access-Control-Max-Age", "3600");
	response.setHeader("Access-Control-Allow-Headers",
		"Content-Type, Accept, X-Requested-With, remember-me, X-Token, Authorization, X-XSRF-TOKEN");
	response.setHeader("access-control-expose-headers",
		"Content-Type, Accept, X-Requested-With, remember-me, X-Token, Authorization, X-XSRF-TOKEN");
	chain.doFilter(req, res);

    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void destroy() {
    }

}