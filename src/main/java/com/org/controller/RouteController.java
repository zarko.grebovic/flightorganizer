package com.org.controller;

import com.org.controller.request.CreateRoute;
import com.org.model.Route;
import com.org.model.User;
import com.org.model.mapper.RouteMapper;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.org.exceptions.RecordNotFoundException;
import com.org.service.RouteService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api")
public class RouteController {

	@Autowired
	RouteService routeService;
	@Autowired
	RouteMapper routeMapper;


	@GetMapping("/findAllRoutes")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<Route>> findAllRoutes() {
		return new ResponseEntity<>(routeService.findAll(), HttpStatus.OK);
	}

	@PostMapping(value = "/createRoute", headers = "Accept=application/json;charset=utf-8")
	public ResponseEntity<?> addRoute(@Valid CreateRoute newRoute) {
		return routeService.createRoute(routeMapper.CreateRouteToRoute(newRoute));
	}
	

}
