package com.org.controller;

import com.org.controller.request.ChangeAvailableTime;
import com.org.controller.request.CreateGate;
import com.org.model.Gate;
import com.org.model.mapper.GateMapper;
import com.org.service.GateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api")
public class GateController {

    @Autowired
    GateService gateService;
    @Autowired
    GateMapper gateMapper;


    @GetMapping("/findAllGates")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<List<Gate>> findAllRoutes() {
        return new ResponseEntity<>(gateService.findAll(), HttpStatus.OK);
    }

    @PostMapping(value = "/createGate", headers = "Accept=application/json;charset=utf-8")
    public ResponseEntity<?> addGate(@Valid CreateGate newGate) {
         return gateService.createGate(gateMapper.fromCreateGateToGate(newGate));
    }

    @Transactional
    @GetMapping("/findAvailableGate")
    public ResponseEntity<Gate> finAvailableGate(@RequestParam(value = "routeNumber", required = true, defaultValue = "") String routeNumber) {
        return new ResponseEntity<>(gateService.findAvailableGate(routeNumber), HttpStatus.OK);
    }


    @PostMapping(value = "/updateGateTime", headers = "Accept=application/json;charset=utf-8")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Gate> updateGateTime(ChangeAvailableTime changeAvailableTime) {
        Gate retVal = gateService.updateGateTimePeriod(changeAvailableTime);
        return new ResponseEntity<>(retVal, HttpStatus.OK);
    }

    @Transactional
    @GetMapping("/markAvailableGate")
    public ResponseEntity<Gate> finAvailableGate(@RequestParam(value = "id", required = true) Long id) {
        return new ResponseEntity<>(gateService.markGateAsAvailable(id), HttpStatus.OK);
    }

}
