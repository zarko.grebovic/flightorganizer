package com.org.controller.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class CreateRoute {

    @NotNull(message = "Airline number cannot be null")
    @Size(min = 2, max = 50, message
            = "Airline number must be between 2 and 50 characters")
    private String airline;

    @NotNull(message = "Source airport cannot be null")
    @Size(min = 3, max = 3, message
            = "Source airport must have 3 characters")
    private String sourceAirport;

    @NotNull(message = "Destination airport cannot be null")
    @Size(min = 3, max = 3, message
            = "Destination airport must have 3 characters")
    private String destinationAirport;

    private Integer stops;

    private BigDecimal price;

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getSourceAirport() {
        return sourceAirport;
    }

    public void setSourceAirport(String sourceAirport) {
        this.sourceAirport = sourceAirport;
    }

    public String getDestinationAirport() {
        return destinationAirport;
    }

    public void setDestinationAirport(String destinationAirport) {
        this.destinationAirport = destinationAirport;
    }

    public Integer getStops() {
        return stops;
    }

    public void setStops(Integer stops) {
        this.stops = stops;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
