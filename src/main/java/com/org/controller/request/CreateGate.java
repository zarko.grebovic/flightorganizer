package com.org.controller.request;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalTime;

public class CreateGate {

    @NotNull(message = "Gate number cannot be null")
    @Size(min = 2, max = 50, message
            = "Gate number must be between 2 and 50 characters")
    private String gateNumber;

    @NotNull(message = "Gate name cannot be null")
    private String gateName;

    private Boolean available;

    @Min(value = 0, message = "Hours should not be less than 0")
    @Max(value = 23, message = "Hours should not be greater than 23")
    private Integer availableFromHours;

    @Min(value = 0, message = "Minutes should not be less than 0")
    @Max(value = 59, message = "Minutes should not be greater than 59")
    private Integer availableFromMinutes;

    @Min(value = 0, message = "Hours should not be less than 0")
    @Max(value = 23, message = "Hours should not be greater than 23")
    private Integer availableToHours;

    @Min(value = 0, message = "Minutes should not be less than 0")
    @Max(value = 59, message = "Minutes should not be greater than 59")
    private Integer availableToMinutes;

    public String getGateNumber() {
        return gateNumber;
    }

    public void setGateNumber(String gateNumber) {
        this.gateNumber = gateNumber;
    }

    public String getGateName() {
        return gateName;
    }

    public void setGateName(String gateName) {
        this.gateName = gateName;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Integer getAvailableFromHours() {
        return availableFromHours;
    }

    public void setAvailableFromHours(Integer availableFromHours) {
        this.availableFromHours = availableFromHours;
    }

    public Integer getAvailableFromMinutes() {
        return availableFromMinutes;
    }

    public void setAvailableFromMinutes(Integer availableFromMinutes) {
        this.availableFromMinutes = availableFromMinutes;
    }

    public Integer getAvailableToHours() {
        return availableToHours;
    }

    public void setAvailableToHours(Integer availableToHours) {
        this.availableToHours = availableToHours;
    }

    public Integer getAvailableToMinutes() {
        return availableToMinutes;
    }

    public void setAvailableToMinutes(Integer availableToMinutes) {
        this.availableToMinutes = availableToMinutes;
    }
}
