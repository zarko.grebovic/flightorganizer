package com.org.controller.request;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class ChangeAvailableTime {

    Long id;

    @Min(value = 0, message = "Hours should not be less than 0")
    @Max(value = 23, message = "Hours should not be greater than 23")
    Integer hoursFrom;

    @Min(value = 0, message = "Minutes should not be less than 0")
    @Max(value = 59, message = "Minutes should not be greater than 59")
    Integer minutesFrom;

    @Min(value = 0, message = "Hours should not be less than 0")
    @Max(value = 23, message = "Hours should not be greater than 23")
    Integer hoursTo;

    @Min(value = 0, message = "Minutes should not be less than 0")
    @Max(value = 59, message = "Minutes should not be greater than 59")
    Integer minutesTo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getHoursFrom() {
        return hoursFrom;
    }

    public void setHoursFrom(Integer hoursFrom) {
        this.hoursFrom = hoursFrom;
    }

    public Integer getMinutesFrom() {
        return minutesFrom;
    }

    public void setMinutesFrom(Integer minutesFrom) {
        this.minutesFrom = minutesFrom;
    }

    public Integer getHoursTo() {
        return hoursTo;
    }

    public void setHoursTo(Integer hoursTo) {
        this.hoursTo = hoursTo;
    }

    public Integer getMinutesTo() {
        return minutesTo;
    }

    public void setMinutesTo(Integer minutesTo) {
        this.minutesTo = minutesTo;
    }
}
