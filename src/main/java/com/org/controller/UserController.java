package com.org.controller;

import com.org.configuration.jwt.SecurityConstants;
import com.org.controller.request.LoginReq;
import com.org.model.Authority;
import com.org.model.User;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import com.org.service.UserService;

import javax.validation.Valid;
import java.util.Date;

@ComponentScan(basePackages = "com")
@RestController
@RequestMapping("/api")
public class UserController {

	private UserService userService;
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	private AuthenticationManager authenticationManager;

	public UserController(AuthenticationManager authenticationManager,
						  UserService userService,
						  BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.authenticationManager = authenticationManager;
		this.userService = userService;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	@PostMapping("/login")
	public ResponseEntity<User> login(@Valid LoginReq request) {
		try {
			Authentication authenticate = authenticationManager
					.authenticate(
							new UsernamePasswordAuthenticationToken(
									request.getUsername(), request.getPassword()
							)
					);

			User user = userService.findByLogin(((UserDetails) authenticate.getPrincipal()).getUsername());

			//Create commaSeparated string for authorities
			StringBuffer roles = new StringBuffer();
			for(Authority authority : user.getAuthorities()){
				if(roles.length() != 0){
					roles.append(",");
				}
				roles.append(authority.getName());
			}

			String token = Jwts.builder()
					.setSubject(request.getUsername())
					.claim(SecurityConstants.AUTHORITIES_KEY, roles.toString())
					.setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
					.signWith(SignatureAlgorithm.HS512, SecurityConstants.SECRET).compact();

			return ResponseEntity.ok()
					.header(
							HttpHeaders.AUTHORIZATION,
							SecurityConstants.TOKEN_PREFIX + token
					)
					.body(user);
		} catch (BadCredentialsException ex) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
	}



//	@PostMapping(value = "/createUser", headers = "Accept=application/json;charset=utf-8")
//	public ResponseEntity<?> addUser(User newUser) {
//		return userService.createUser(newUser);
//	}

}