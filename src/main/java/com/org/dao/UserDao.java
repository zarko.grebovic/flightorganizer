package com.org.dao;

import com.org.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends BaseRepository<User, Long>{

    User findByLogin(String username);

}