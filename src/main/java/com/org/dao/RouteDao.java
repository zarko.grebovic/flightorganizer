package com.org.dao;


import com.org.model.Route;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RouteDao extends BaseRepository<Route, Long>{

    List<Route> findAll();

    Route findByAirlineIgnoreCase(String flightNumber);
}
