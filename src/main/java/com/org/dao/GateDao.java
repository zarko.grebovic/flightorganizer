package com.org.dao;

import com.org.model.Gate;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Time;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;


@Repository
public interface GateDao extends BaseRepository<Gate, Long>{

    List<Gate> findAll();

    @Query(value = "SELECT * FROM GATE WHERE (AVAILABLE IS NULL OR AVAILABLE = TRUE) AND (AVAILABLE_TO IS NULL OR " +
            "(HOUR(AVAILABLE_TO) > HOUR(:currentTime) OR (HOUR(AVAILABLE_TO) = HOUR(:currentTime) AND (MINUTE(AVAILABLE_TO) >= MINUTE(:currentTime)))) " +
            "AND (HOUR(AVAILABLE_FROM) < HOUR(:currentTime) OR (HOUR(AVAILABLE_FROM) = HOUR(:currentTime) AND (MINUTE(AVAILABLE_FROM) <= MINUTE(:currentTime))))) LIMIT 1", nativeQuery = true)
    Optional<Gate> findFirstAvailableGate(@Param("currentTime") Time currentTime);


}
