package com.org.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "GATE")
public class Gate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Long version;

    @Column(name = "gate_number")
    private String gateNumber;

    @Column(name = "gate_name")
    private String gateName;

    @Column(name = "available")
    private Boolean available;

    @Column(name = "available_from")
    private Time availableFrom;

    @Column(name = "available_to")
    private Time availableTo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGateNumber() {
        return gateNumber;
    }

    public void setGateNumber(String gateNumber) {
        this.gateNumber = gateNumber;
    }

    public String getGateName() {
        return gateName;
    }

    public void setGateName(String gateName) {
        this.gateName = gateName;
    }

    public Date getAvailableFrom() {
        return availableFrom;
    }

    public void setAvailableFrom(Time availableFrom) {
        this.availableFrom = availableFrom;
    }

    public Time getAvailableTo() {
        return availableTo;
    }

    public void setAvailableTo(Time availableTo) {
        this.availableTo = availableTo;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Gate gate = (Gate) o;
        return Objects.equals(id, gate.id) &&
                Objects.equals(gateNumber, gate.gateNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, gateNumber);
    }
}
