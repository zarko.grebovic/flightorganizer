package com.org.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A Route.
 */
@Entity
@Table(name = "ROUTE")
public class Route implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Long version;

    @Column(name = "airline")
    private String airline;

    @Column(name = "source_airport")
    private String sourceAirport;

    @Column(name = "destination_airport")
    private String destinationAirport;

    @Column(name = "stops")
    private Integer stops;

    @Column(name = "price", precision = 21, scale = 2)
    private BigDecimal price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAirline() {
        return airline;
    }

    public Route() {
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getSourceAirport() {
        return sourceAirport;
    }

    public void setSourceAirport(String sourceAirport) {
        this.sourceAirport = sourceAirport;
    }

    public String getDestinationAirport() {
        return destinationAirport;
    }

    public void setDestinationAirport(String destinationAirport) {
        this.destinationAirport = destinationAirport;
    }

    public Integer getStops() {
        return stops;
    }

    public void setStops(Integer stops) {
        this.stops = stops;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Route route = (Route) o;
        return Objects.equals(id, route.id) &&
                Objects.equals(airline, route.airline) &&
                Objects.equals(sourceAirport, route.sourceAirport) &&
                Objects.equals(destinationAirport, route.destinationAirport);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, airline, sourceAirport, destinationAirport);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Route{" +
            "id=" + getId() +
            ", airline='" + getAirline() + "'" +
            ", sourceAirport='" + getSourceAirport() + "'" +
            ", destinationAirport='" + getDestinationAirport() + "'" +
            ", stops=" + getStops() +
            ", price=" + getPrice() +
            "}";
    }
}
