package com.org.model.mapper;

import com.org.controller.request.CreateRoute;
import com.org.model.Route;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RouteMapper {

    Route CreateRouteToRoute(CreateRoute request);

}
