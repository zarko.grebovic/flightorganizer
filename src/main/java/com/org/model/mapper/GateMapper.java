package com.org.model.mapper;

import com.org.controller.request.CreateGate;
import com.org.model.Gate;
import org.mapstruct.Mapper;

import java.sql.Time;
import java.time.LocalTime;

@Mapper(componentModel = "spring")
public interface GateMapper {

    Gate CreateGatetoGate(CreateGate request);

    default Gate fromCreateGateToGate(CreateGate request){
        Gate retVal = CreateGatetoGate(request);
        if(request.getAvailableFromHours() != null && request.getAvailableFromMinutes() != null){
            retVal.setAvailableFrom(Time.valueOf(LocalTime.of(request.getAvailableFromHours(), request.getAvailableFromMinutes())));
        }
        if(request.getAvailableToHours() != null && request.getAvailableToMinutes() != null){
            retVal.setAvailableTo(Time.valueOf(LocalTime.of(request.getAvailableToHours(), request.getAvailableToMinutes())));
        }
        return retVal;
    }

}
