package com.org.service.impl;

import com.org.controller.request.ChangeAvailableTime;
import com.org.dao.GateDao;
import com.org.dao.RouteDao;
import com.org.exceptions.RecordAlreadyPresentException;
import com.org.exceptions.RecordNotFoundException;
import com.org.model.Gate;
import com.org.model.Route;
import com.org.service.GateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Time;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;


@Service
public class GateServiceImpl implements GateService {

    @Autowired
    GateDao gateDao;
    @Autowired
    RouteDao routeDao;

    @Override
    public List<Gate> findAll() {
        return gateDao.findAll();
    }

    @Override
    @Transactional
    public Gate findAvailableGate(String routeNumber){
        Route checkIfExistRoute = routeDao.findByAirlineIgnoreCase(routeNumber);
        if(checkIfExistRoute == null){
            throw new RecordNotFoundException("Flight with entered number not exists");
        }
        Time now = Time.valueOf(LocalTime.now());
        Optional<Gate> retVal = gateDao.findFirstAvailableGate(now);
        if(retVal.isPresent()){
            Gate currentGate = retVal.get();
            currentGate.setAvailable(Boolean.FALSE);
            return gateDao.save(currentGate);
        }else{
            throw new RecordNotFoundException("Currently, there not available gates for airplane");
        }
    }

    @Override
    @Transactional
    public ResponseEntity<?> createGate(Gate newGate) {
        Optional<Gate> findGateById = gateDao.findById(newGate.getId());
        try {
            if (!findGateById.isPresent()) {
                gateDao.save(newGate);
                return new ResponseEntity<>(newGate, HttpStatus.OK);
            } else
                throw new RecordAlreadyPresentException(
                        "Gate with Id: " + newGate.getId() + " already exists!!");
        } catch (RecordAlreadyPresentException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @Override
    @Transactional
    public Gate markGateAsAvailable(Long id){
        Gate retVal = gateDao.findOne(id);
        retVal.setAvailable(Boolean.TRUE);
        return gateDao.save(retVal);
    }

    @Override
    @Transactional
    public Gate updateGateTimePeriod(ChangeAvailableTime changeAvailableTime){
        Gate retVal = gateDao.findOne(changeAvailableTime.getId());
        retVal.setAvailableFrom(Time.valueOf(LocalTime.of(changeAvailableTime.getHoursFrom(), changeAvailableTime.getMinutesFrom())));
        retVal.setAvailableTo(Time.valueOf(LocalTime.of(changeAvailableTime.getHoursTo(), changeAvailableTime.getMinutesTo())));
        return gateDao.save(retVal);
    }



}
