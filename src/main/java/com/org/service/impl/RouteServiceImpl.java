package com.org.service.impl;

import java.util.List;
import java.util.Optional;

import com.org.dao.RouteDao;
import com.org.exceptions.RecordAlreadyPresentException;
import com.org.model.Route;
import com.org.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;


@Service
public class RouteServiceImpl implements RouteService {

	@Autowired
	RouteDao routeDao;

	@Override
	@Transactional
	public ResponseEntity<?> createRoute(Route newRoute) {
		Optional<Route> findRouteById = routeDao.findById(newRoute.getId());
		try {
			if (!findRouteById.isPresent()) {
				routeDao.save(newRoute);
				return new ResponseEntity<>(newRoute, HttpStatus.OK);
			} else
				throw new RecordAlreadyPresentException(
						"Route with Id: " + newRoute.getId() + " already exists!!");
		} catch (RecordAlreadyPresentException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}

	@Override
	public List<Route> findAll() {
		return routeDao.findAll();
	}


}
