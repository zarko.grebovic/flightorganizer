package com.org.service.impl;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.org.model.Authority;
import com.org.model.User;
import com.org.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.org.dao.UserDao;
import com.org.exceptions.RecordAlreadyPresentException;

import javax.transaction.Transactional;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {

	@Autowired
	UserDao userDao;

	
	@Override
	@Transactional
	public ResponseEntity<?> createUser(User newUser) {
		Optional<User> findUserById = userDao.findById(newUser.getId());
		try {
			if (!findUserById.isPresent()) {
				userDao.save(newUser);
				return new ResponseEntity<>(newUser, HttpStatus.OK);
			} else
				throw new RecordAlreadyPresentException(
						"User with Id: " + newUser.getId() + " already exists!!");
		} catch (RecordAlreadyPresentException e) {

			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}



	@Override
	public User findByLogin(String username) {
		return userDao.findByLogin(username);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User applicationUser = userDao.findByLogin(username);
		if (applicationUser == null) {
			throw new UsernameNotFoundException(username);
		}
        List<GrantedAuthority> roles = new ArrayList<>(applicationUser.getAuthorities().size());
		for(Authority authority : applicationUser.getAuthorities()){
            GrantedAuthority auth = new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return authority.getName();
                }
            };
            roles.add(auth);
        }
		return new org.springframework.security.core.userdetails.User(applicationUser.getLogin(), applicationUser.getPassword(), roles);
	}

}