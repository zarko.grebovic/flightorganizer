package com.org.service;


import com.org.model.User;
import org.springframework.http.ResponseEntity;

public interface UserService{

	 ResponseEntity<?> createUser(User newUser);

	 User findByLogin(String username);
}