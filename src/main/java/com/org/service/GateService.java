package com.org.service;

import com.org.controller.request.ChangeAvailableTime;
import com.org.model.Gate;
import org.springframework.http.ResponseEntity;

import java.util.List;


public interface GateService {

     List<Gate> findAll();

     Gate findAvailableGate(String routeNumber);

     Gate updateGateTimePeriod(ChangeAvailableTime changeAvailableTime);

     Gate markGateAsAvailable(Long id);

     ResponseEntity<?> createGate(Gate newGate);

}
