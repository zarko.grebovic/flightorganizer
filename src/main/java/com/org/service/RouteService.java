package com.org.service;

import java.math.BigInteger;
import java.util.List;


import com.org.exceptions.RecordNotFoundException;
import com.org.model.Route;
import org.springframework.http.ResponseEntity;

public interface RouteService {


	List<Route> findAll();

	ResponseEntity<?> createRoute(Route newRoute);

}
