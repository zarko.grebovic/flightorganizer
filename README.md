- Development of application has been worked in Spring Boot, Spring data and Hibernate.

- Users:

    1. Administrator - username: admin password: user  
    2. Regular user  - username: user password: user

- Swagger url: http://localhost:8181/swagger-ui.html

    Note: Swagger response problem - no content for validation errors, in network tab response is visible.


- After login api call authorization (JWT) token shoud be copied and all other methods need to called with that token.
    example: "authorization": "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiYXV0aG9yaXRpZXMiOiJST0xFX1VTRVIiLCJleHAiOjE2MDgwNzA2MDZ9.jtUQ356KTRA7Iyau0KA1ZN5bsysmM3UnZ5xfV3uDkITFCHueZ2GtP90xHgtfc4LEuuSyo8OYlLVMLdbdTYBckw"

- Error handling - @ControllerAdvice used for exceptions (GlobalExceptionHandler.java)

- Liquibase - 10 rows already inserted for routes and gates. Administrator can see response from methods: findAllRoutes and findAllGates.

- Dev DB - H2 in-memory database used for development(DB params are available in application.properties) is available on http://localhost:8181/h2

- Mapstruct - Because Mapstruct is used in project should run mvn clean install to generate Mapstruct implementation

- Methods available only for Administrator role: findAllGates, updateGateTime, findAllRoutes.

- Extra Challenge - implemented method updateGateTime to update gate time.